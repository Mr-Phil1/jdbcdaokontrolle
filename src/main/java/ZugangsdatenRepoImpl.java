import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ZugangsdatenRepoImpl implements ZugangsdatenRepository {
    private final Connection conn;

    public ZugangsdatenRepoImpl() throws SQLException, ClassNotFoundException {
        conn = MySqLDBVerbindung.getConnection("jdbc:mysql://localhost:3306/zugangsdaten", "root", "123");
    }

    @Override
    public Optional<Zugangsdaten> insert(Zugangsdaten entity) {

        String sql = "INSERT INTO `zugangsdaten` ( `username`, `password`, `url`) VALUES ( ?, ?, ?) ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getPasswort());
            ps.setString(3, entity.getUrl());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedEntities = ps.getGeneratedKeys();
            if (generatedEntities.next()) {
                return this.getById(generatedEntities.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new DatenbankException(e.getMessage());
        }
    }

    @Override
    public Optional<Zugangsdaten> getById(Long id) {
        EigeneAsserts.notNull(id);
        String sql = "SELECT * FROM `zugangsdaten` WHERE `id`=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            Zugangsdaten zg = new Zugangsdaten(rs.getLong("id"), rs.getString("username"), rs.getString("password"), rs.getString("url"));
            return Optional.of(zg);
        } catch (SQLException e) {
            throw new DatenbankException(e.getMessage());
        }
    }

    @Override
    public List<Zugangsdaten> getAll() {
        String sql = "SELECT * FROM `zugangsdaten`";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ArrayList<Zugangsdaten> zgList = new ArrayList<>();
            while (rs.next()) {
                zgList.add(new Zugangsdaten(rs.getLong("id"), rs.getString("username"), rs.getString("password"), rs.getString("url")));
            }
            return zgList;
        } catch (SQLException e) {
            throw new DatenbankException(e.getMessage());
        }
    }

    @Override
    public List<Zugangsdaten> getAllWithUrlContaining(String urlpart) {
        EigeneAsserts.notNull(urlpart);
        String sql = "SELECT * FROM `zugangsdaten` WHERE LOWER(`url`) LIKE LOWER(?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + urlpart + "%");
            ResultSet rs = ps.executeQuery();
            ArrayList<Zugangsdaten> zgList = new ArrayList<>();
            while (rs.next()) {
                zgList.add(new Zugangsdaten(rs.getLong("id"), rs.getString("username"), rs.getString("password"), rs.getString("url")));
            }
            return zgList;
        } catch (SQLException e) {
            throw new DatenbankException(e.getMessage());

        }
    }
}
